import cv2
import numpy as np
from PIL import Image
import os

# Build Path for face image repository
path = "C:\\Users\\me\\Desktop\\detection\\comparing\\yale_rep"
recognizer = cv2.face.LBPHFaceRecognizer_create()
detector = cv2.CascadeClassifier("C:\\Users\\me\\Desktop\\detection\\comparing\\classifier\\haarcascade_frontalface_default.xml")

# function to get the pictures and label the data
def getImagesAndLabels(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]     
    faceSamples=[]
    ids = []
    for imagePath in imagePaths:
        PIL_img = Image.open(imagePath).convert('L') # grayscale
        img_numpy = np.array(PIL_img,'uint8')
        id = int(os.path.split(imagePath)[1].split('.')[0])
       # id = int(os.path.split(imagePath)[-1].split(".")[1])
        faces = detector.detectMultiScale(img_numpy)
        for (x,y,w,h) in faces:
            faceSamples.append(img_numpy[y:y+h,x:x+w])
            ids.append(id)
    return faceSamples,ids

print ("\n [INFO] Training faces. It will take a few seconds. Wait ...")
faces,ids = getImagesAndLabels(path)
#ids,faces= getImagesAndLabels(path)
recognizer.train(faces, np.array(ids))

# Save the model into trainer.yml
recognizer.write("C:\\Users\\me\\Desktop\\detection\\comparing\\trainer1.yml")

# Print the numer of faces trained
print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(ids))))
