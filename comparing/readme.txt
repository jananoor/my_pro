
COMPARING DIFFERENT ALGORITHM IN FACE DETECTION AND RECOGNITION IN PYTHON BY USING OPENCV

-------------------------------------------------------------------------------------------
                          Created by - RIZWANA NAZ ASIF
                   Department of Design Engineering and Mathematics
                         Middlesex University Hendon, London
                                 ra1181@live.mdx.ac.uk
			  Supervisor - Eris Chinellato
-------------------------------------------------------------------------------------------

I     -    Aim and Objective-

To detect and recognise the face in still image, train them and compare the results of the two different algorithms.
To work in the python language by using OpenCV.

      - Software Documentation -
   		1.  Codes
			>Facedetector-HAAR-cascade
			>Face detector-LBPH
			>Trainer
			>Recogniser
		
   		2.  Databases
  			>Yale Face Database
			>Naz Face Database
	
   		3.  Classifier
			> Haar Cascade Classifier
			> LBPH Classifier

II  -  Description

    1 Face Detection by Using Haar Cascade
    2 face detection and recognition by using LBPH

First step to install opencv.
	pip install opencv-python
other dependencies are installed as well when required.

I used face detection by using Haar cascade and the LBPH (machine learning approach). Here the cascade function trained with input data. 
Opencv already having some pre-trained classifier for face, eyes, etc. I worked on face and eye classifier.
From Opencv github repository, I downloaded trained classifier xml file 
(haarcascade_frontalface_default.xml and LBPH_frontalface_default.xml) and saved in the working folder.

Detect faces in images:

First to convert in the grey scale image. It is mandatory part to convert colour image to grayscale image.
DetectMultiScale () function in the line below is used to detect the face and it is taking three arguments: 
faces = face_cascade.detectMultiScale(gray, 1.1, 4)
						input image      scale factor         min neighbours
	•	Image: matrix of the type cv-84 containing an image where the objects are detected.
	•	Objects: vector of rectangles where each rectangle contains the detected object, 
		the rectangles may be particularly outside the original image.
	•	scalefactor: it tells how much the image size reduced with each scale.
	•	minNeighbour: it tells how many neighbours each rectangle candidate should have to retain.

Faces have list of coordinates for the rectangular region where the face found. We can use these coordinates to draw rectangle in our image.
There is one file in which I detect faces in video as well.

Datasets:
Two datasets are being used for the training and detecting the iamges with different gestres and intesity of light.

			The Yale Face Database
			----------------------

The database contains 180 GIF images of 15 subjects (subject01, 
subject02, etc.).  There are 12 images per subject, one  for each 
of the following facial expressions or configurations: center-light, 
wear glasses, happy, left-light, without glasses, normal, right-light,
head slightly down,sad, sleepy, surprised, and wink.  


All the trained images are available in yale_rep and in the test data 
the other images are available which are going to test against  

			The Naz Face Database
			----------------------
this database has 7 subjects with 2o images each, means altogether they contain 140 images (gif, jpg, png). 
we have the varity of different iamges like group images, colour images and greyscale images as well.
All the images have the specific IDs during the trainig as it is convenient for trainer code.  






                               End of the session!
------------------------------------------------------------------------------------------------------------