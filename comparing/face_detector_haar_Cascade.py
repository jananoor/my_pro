import cv2

# Give the path for building the image path
imagePath = "C:\\Users\\me\\Desktop\\detection\\comparing\\testdata\\riz3.png"
cascPath = "C:\\Users\\me\\Desktop\\detection\\comparing\\classifier\\haarcascade_frontalface_default.xml"

# Create the haar cascade classifier
faceCascade = cv2.CascadeClassifier(cascPath)

# Read the image from the path given in image path
image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detect faces in the image provided in image path
faces = faceCascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

print("Found {0} faces!".format(len(faces)))

# Draw a green colour box around the face to identify
for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imwrite("C:\\Users\\me\\Desktop\\detection\\images\\Faces_found_CASCADE.jpg", image)
cv2.imshow("Faces_found_CASCADE", image)
cv2.waitKey(0)

