import cv2
import numpy as np


recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("C:\\Users\\me\\Desktop\\detection\\comparing\\trainer1.yml")
#cascadePath ="C:\\Users\\me\\Desktop\\detection\\comparing\\classifier\\haarcascade_frontalface_default.xml"
cascadePath = "C:\\Users\\me\\Desktop\\detection\\comparing\\classifier\\lbpcascade_frontalface_default.xml"
#cascadePath = "C:\\Users\\me\\Desktop\\detection\\comparing\\classifier\\lbpcascade_frontalface.xml"

faceCascade = cv2.CascadeClassifier(cascadePath)

font = cv2.FONT_HERSHEY_SIMPLEX
#iniciate id counter
id = 0

# names related to ids: example ==> Einstien: id=1,  etc
names = ['SUNNY', 'EINSTEIN', 'ESHA', 'SARA','ZAR','SUSHIL','TOM','r','g','e']

#read the image
img = cv2.imread("C:\\Users\\me\Desktop\\detection\\comparing\\testdata\\c.jpg")
 
#convert to the gray     
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
faces = faceCascade.detectMultiScale( 
        gray,
        scaleFactor = 1.1,
        minNeighbors = 5,
        minSize=(50, 50))
print("NO OF FACES DETECTED = ",len(faces))
for(x,y,w,h) in faces:
    cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 2)
    id, confidence = recognizer.predict(gray[y:y+h,x:x+w])
    print(id, confidence)
    if (confidence < 100):
        id = names[id]
        confidence = "  {0}%".format(round(100 - confidence))
    else:
        id = "UNKNOWN"
        confidence = "  {0}%".format(round(100 - confidence))
        
    cv2.putText(img, str(id), (x+5,y-5), font, 1, (255,255,255), 2)
    cv2.putText(img, str(confidence),(x+5,y+h-5),font, 1, (255,255,0), 1)  
    
cv2.imshow('RECOGNIZED',img) 
k = cv2.waitKey(0) & 0xff 
cv2.destroyAllWindows()
